# Network utilities container image

Useful for debugging network issues.

## Usage

Create an ad-hoc "net-debug" pod in the default namespace on the pod network:

```sh
$ kubectl run -it --rm -n default \
    --image=registry.ethz.ch/hpc-registry/netutils net-debug
root@debug:/# nslookup kubernetes.default
Server:         10.205.209.10
Address:        10.205.209.10#53

Name:   kubernetes.default.svc.cluster.local
Address: 10.205.209.1
```

The same on the host network:

```sh
$ kubectl run -it --rm -n default --overrides='{"spec":{"hostNetwork":true}}' \
    --image=registry.ethz.ch/hpc-registry/netutils net-debug
root@eu-k8s-dev-10:/# ip a show access
8: access: <BROADCAST,MULTICAST,MASTER,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
    link/ether d6:6a:36:07:83:02 brd ff:ff:ff:ff:ff:ff
    inet 10.205.160.40/20 brd 10.205.175.255 scope global access
       valid_lft forever preferred_lft forever
```

### Using iperf helm chart

By default it deploys a DaemonSet. Feel free to create a custom
`values.custom.yaml` file in charts/iperf/ directory and override any defaults
in [charts/iperf/values.yaml](charts/iperf/values.yaml).

Then deploy:

```sh
HELM_ARGS="-f ./values.custom.yaml" make -C charts/iperf install
```

Choose your nodes:

```sh
src_node=eu-k8s-dev-10.euler.ethz.ch
dst_node=eu-g3-082.euler.ethz.ch
```

And run the test:

```sh
kubectl exec -t -n default \
    "$(kubectl get pods -n default -o name \
        --field-selector=spec.nodeName=="$src_node")" -- \
    iperf3 --bidir -P 4 -4 -t 30 --client \
        "$(kubectl get pods -n default -l app.kubernetes.io/name=iperf-server \
            --field-selector=spec.nodeName="$dst_node" \
            --output=go-template='{{range .items}}{{.status.podIP}}{{"\n"}}{{end}}')"
```

Cleanup:

```sh
make -C charts/iperf uninstall
```

### Iperf server-client

An ad-hoc pod on a specific node running iperf server:

```sh
$ kubectl run --attach --rm -n default \
    --overrides='{"spec":{"nodeName":"eu-k8s-dev-09.euler.ethz.ch"}}' \
    --image=registry.ethz.ch/hpc-registry/netutils \
    iperf-server --command -- iperf3 --server -4 --one-off
```

*Note*: The pod will terminate and will be deleted after the first handled
connection thanks to `--one-off`.

Determine its IP:

```sh
$ kubectl get pod -o wide -n default iperf-server
NAME           READY   STATUS    RESTARTS   AGE    IP               NODE                          NOMINATED NODE   READINESS GATES
iperf-server   1/1     Running   0          4m2s   10.205.244.143   eu-k8s-dev-09.euler.ethz.ch   <none>           <none>
```

And run a client on a different node (on pod network):

```sh
$ kubectl run --attach --rm -n default \
    --overrides='{"spec":{"nodeName":"eu-k8s-dev-10.euler.ethz.ch"}}' \
    --image registry.ethz.ch/hpc-registry/netutils \
    iperf-client --command -- iperf3 --bidir -P 4 -4 --client 10.205.244.143
...
[ 17][RX-C]   0.00-10.00  sec  3.79 GBytes  3.25 Gbits/sec  2725             sender
[ 17][RX-C]   0.00-10.00  sec  3.79 GBytes  3.25 Gbits/sec                  receiver
[ 19][RX-C]   0.00-10.00  sec  3.81 GBytes  3.27 Gbits/sec  3012             sender
[ 19][RX-C]   0.00-10.00  sec  3.80 GBytes  3.27 Gbits/sec                  receiver
[SUM][RX-C]   0.00-10.00  sec  22.2 GBytes  19.1 Gbits/sec  11059             sender
[SUM][RX-C]   0.00-10.00  sec  22.2 GBytes  19.0 Gbits/sec                  receiver

iperf Done.
pod "iperf-client" deleted
```
