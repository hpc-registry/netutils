# renovate: datasource=docker
ARG UBUNTU_IMAGE_TAG=24.04@sha256:80dd3c3b9c6cecb9f1667e9290b3bc61b78c2678c02cbdae5f0fea92cc6734ab
FROM ubuntu:${UBUNTU_IMAGE_TAG}

ARG WITH_IPERF=1

# renovate: datasource=repology depName=ubuntu_24_04/iperf3 versioning=semver-coerced
ARG IPERF3_VERSION=3.16
# renovate: datasource=repology depName=ubuntu_24_04/iproute2 versioning=semver-coerced
ARG IPROUTE2_VERSION=6.1.0
# renovate: datasource=repology depName=ubuntu_24_04/bind9 versioning=semver-coerced
ARG BIND9_UTILS_VERSION=1:9.18.28
# renovate: datasource=repology depName=ubuntu_24_04/curl versioning=semver-coerced
ARG CURL_VERSION=8.5.0
# renovate: datasource=repology depName=ubuntu_24_04/nmap versioning=semver-coerced
ARG NMAP_VERSION=7.94+git20230807.3be01efb1+dfsg
# renovate: datasource=repology depName=ubuntu_24_04/tini versioning=semver-coerced
ARG TINI_VERSION=0.19.0
# renovate: datasource=repology depName=ubuntu_24_04/traceroute versioning=semver-coerced
ARG TRACEROUTE_VERSION=1:2.1.5
# renovate: datasource=repology depName=ubuntu_24_04/inetutils-ping versioning=semver-coerced
ARG INETUTILS_PING_VERSION=2:2.5
# renovate: datasource=repology depName=ubuntu_24_04/iftop versioning=semver-coerced
ARG IFTOP=1.0~pre4
# renovate: datasource=repology depName=ubuntu_24_04/tcpdump versioning=semver-coerced
ARG TCPDUMP_VERSION=4.99.4
# renovate: datasource=repology depName=ubuntu_24_04/mtr-tiny versioning=semver-coerced
ARG MTR_TINY_VERSION=0.95
# renovate: datasource=repology depName=ubuntu_24_04/iputils-tracepath versioning=semver-coerced
ARG IPUTILS_TRACEPATH=3:20240117

# renovate: datasource=docker
ARG UBUNTU_IMAGE_TAG=24.04
ARG CREATED=""
ARG REVISION=""

# hadolint ignore=DL3015
RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        bind9-dnsutils="${BIND9_UTILS_VERSION}-*" \
        bind9-utils="${BIND9_UTILS_VERSION}-*" \
        curl="${CURL_VERSION}-*" \
        inetutils-ping="${INETUTILS_PING_VERSION}-*" \
        iproute2="${IPROUTE2_VERSION}-*" \
        iputils-tracepath="${IPUTILS_TRACEPATH}-*" \
        mtr-tiny="${MTR_TINY_VERSION}-*" \
        nmap="${NMAP_VERSION}-*" \
        tcpdump="${TCPDUMP_VERSION}-*" \
        tini="${TINI_VERSION}-*" \
        traceroute="${TRACEROUTE_VERSION}-*" \
 && if [ "${WITH_IPERF:-0}" = "1" ]; then apt-get install -y iperf3="${IPERF3_VERSION}-*"; fi \
 && rm -rf /var/cache/apt/*

ENTRYPOINT ["/usr/bin/tini", "/bin/bash", "--"]

LABEL org.opencontainers.image.authors "Michal Minář <michal.minar@id.ethz.ch>"
LABEL org.opencontainers.image.source "https://gitlab.ethz.ch/hpc-registry/netutils"
LABEL org.opencontainers.image.url="https://gitlab.ethz.ch/hpc-registry/netutils"
LABEL org.opencontainers.image.title "Network utilities"
LABEL org.opencontainers.image.base="docker.io/ubuntu:${UBUNTU_IMAGE_TAG}"
LABEL org.opencontainers.image.licenses="AGPL-3.0-or-later"
LABEL org.opencontainers.image.created="${CREATED}"
LABEL org.opencontainers.image.revision="${REVISION}"
