NAMESPACE    ?= default
CHART_NAME   ?= iperf-server
HELM_ARGS    := $(CHART_NAME) . -n $(NAMESPACE) -f values.yaml $(HELM_ARGS)

.PHONY: lint
lint:
	make render | awk -v o=/dev/stderr '/^(apiVersion:|---)/ { o="/dev/stdout" } { print >o }' | \
		kube-linter lint --fail-if-no-objects-found --fail-on-invalid-resource -

.PHONY: render
render:
	helm template $(HELM_ARGS)

.PHONY: install
install:
	helm install --create-namespace $(HELM_ARGS)

upgrade:
	helm upgrade $(HELM_ARGS)

.PHONY: diff
# requires helm diff plugin
diff:
	helm diff upgrade $(HELM_ARGS)

.PHONY: kdiff
kdiff:
	make render | awk -v o=/dev/stderr '/^(apiVersion:|---)/ { o="/dev/stdout" } { print >o }' | \
		kubectl diff -n "$(NAMESPACE)" -f - ||:

.PHONY: uninstall
uninstall:
	helm uninstall -n "$(NAMESPACE)" "$(CHART_NAME)"

# ex: ft=make noet ts=4 sw=4 :
